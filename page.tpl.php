<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body class="<?php print $section_class . ' ' . $type_class; ?>" id="<?php print $body_id; ?>">

<div id="wrapper"> <!--width independent from body-->

	

	<div id="header">	  	

 		  <?php if ($logo): ?>
	        <div id="logo"><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a></div>
	      <?php endif; ?> 
	
		  <div id="navigation">
			  	<?php if (isset($primary_links)) : ?>
		          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
		        <?php endif; ?>

		        <?php if (isset($secondary_links)) : ?>
		          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
		        <?php endif; ?>	
		  </div>
	
		  <?php if ($site_name): ?>
			<h1 id="site-name"><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
		       <?php print $site_name; ?> </a>
			</h1>
		  <?php endif; ?> 
		  <?php if ($site_slogan): ?>
			<h2 id="slogan">
		       <?php print $site_slogan; ?>
			</h1>
		  <?php endif; ?>
		
		 
		  
		
		  <?php if ($header || $search_box): ?>
	          <?php print $header ?>
	          <?php print $search_box ?>
	      <?php endif; ?>
	  
	</div> <!-- end header -->
	
	

	<div id="main">
		
	  <?php if ($sidebar_left): ?>
	    <div id="sidebar-left">
		    <?php print $sidebar_left ?>
	    </div>
	  <?php endif; ?>	
	
	  <div id="maincontent">
		
		    <?php print $messages ?>
		
		    <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
		
		   	<div class="tabs"><?php print $tabs; ?></div>
		
			<?php print $content; ?>
	  </div>		
	  
	  <?php if ($sidebar_right): ?>
	    <div id="sidebar-right">
		    <?php print $sidebar_right ?>
	    </div>
	  <?php endif; ?>

	</div>
	
	<?php if ($footer_message): ?>
	    <div id="footer">
		    <?php print $footer_message ?>
	    </div>
	<?php endif; ?>

	<?php print $closure ?>
</div>

</body>
</html>