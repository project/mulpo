<?php


/**
 * Regions
*/

function mulpo_regions() {
  return array(
    'left' => t('left sidebar'),
    'right' => t('right sidebar'),
    'content' => t('content'),
    'header' => t('header'),
    'footer' => t('footer'),
  );
}

/**
 * Make additional variables available and override some core variables
 */
function _phptemplate_variables($hook, $vars) {
  switch ($hook) {
    case 'page':
      // generate a section variable as similar to:
      // http://drupal.org/node/16383
      // http://drupal.org/node/32077
      $uri_path = trim($_SERVER['REQUEST_URI'], '/');
      $uri_parts = explode('/', $uri_path);
      $uri_parts_clean = explode('?', $uri_parts[0]);      
      $vars['subsection'] = count($uri_parts);
      $vars['body_id'] = ($vars['is_front']) ? 'front' : str_replace(array('/', '?','='),'-', $uri_path);
      $vars['section_class'] = (empty($uri_parts_clean[0])) ? 'front' : $uri_parts_clean[0];
      $vars['type_class'] = isset($vars['node']) ?  $vars['node']->type .' ' : '';

	  if(module_exists('javascript_aggregator') && $vars['scripts']) {
	      $vars['scripts'] = javascript_aggregator_cache($vars['scripts']);
	    }
    break;
  }
  return $vars;
}

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($sidebar_left, $sidebar_right) {
  if ($sidebar_left != '' && $sidebar_right != '') {
    $class = 'sidebars';
  }
  else {
    if ($sidebar_left != '') {
      $class = 'sidebar-left';
    }
    if ($sidebar_right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
// uncomment the next line to enable current page in the breadcrumb trail
//    $breadcrumb[] = drupal_get_title();
    return '<div class="breadcrumb">'. implode(' » ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}



/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}

/**
 * Generates IE CSS links.
 */
function phptemplate_get_ie_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie.css" />';
  return $iecss;
}

function phptemplate_get_ie6_styles() {
  $iecss = '<link type="text/css" rel="stylesheet" media="all" href="'. base_path() . path_to_theme() .'/fix-ie6.css" />';
  return $iecss;
}

/**
 * Adds even and odd classes to <li> tags in ul.menu lists
 */
function phptemplate_menu_item($mid, $children = '', $leaf = true) {
static $count = 0;
$zebra = ($count % 2) ? 'odd' : 'even';
$count++;
return '<li class="' . ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) .' ' . $zebra . '">'. menu_item_link($mid) . $children ."</li>\n";
}

/**
* Fixes illegal duplicate html id's "edit-sumit".
*/
function phptemplate_submit($element) {
  static $dupe_ids = array();
  if (isset($dupe_ids[$element['#id']])) {
    $dupe_ids[$element['#id']]++;
    $element['#id'] = $element['#id'] .'-'. $dupe_ids[$element['#id']];
  }
  else {
    $dupe_ids[$element['#id']] = 0;
  }
  return theme('button', $element);
}